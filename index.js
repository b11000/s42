// DOM 
// Document
// Object 
// Model
// "console.log(document)" para makita nyo na si document ay yung mismong webpage
// Retrieve an element from the webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
// "document" refers to the whole webpage and "querySelector" is used to select a specific object (HTML element) from the document.

/* Alternatively, we can use getElement function to retrieve the elements
    document.getElementById('txt-first-name);
    document.getElementByClassName('txt-inputs');
    document.getElementByTagName('input');
*/

// Activity Solution
// const txtFirstName = document.querySelector('#txt-first-name');
// const txtLastName = document.querySelector('#txt-last-name');
// const spanFullName = document.querySelector('#span-full-name');

// const updateFullName = ()=>{
//     let firstName = txtFirstName.value;
//     let lastName = txtLastName.value;

//     spanFullName.innerHTML = `${firstName} ${lastName}`; 
// }

// txtLastName.addEventListener('keyup', updateFullName);
// txtFirstName.addEventListener('keyup', updateFullName);

// Performs an action when an event triggers
txtFirstName.addEventListener("keyup", (event) => {
    spanFullName.innerHTML =
        `${txtFirstName.value} ${txtLastName.value}`;
});

txtLastName.addEventListener("keyup", (event) => {
    spanFullName.innerHTML =
        `${txtFirstName.value} ${txtLastName.value}`;
});

// Performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value;

    console.log(event.target);
    console.log(event.target.value);
})
// Performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
    // spanFullName.innerHTML = txtFirstName.value;

    // event.target contains the element where the event happened
    console.log(event.target);
    // event.target.value gets the value of the input object
    console.log(event.target.value);
})

txtLastName.addEventListener('keyup', (event) => {
    // spanFullName.innerHTML = txtLastName.value;

    // event.target contains the element where the event happened
    console.log(event.target);
    // event.target.value gets the value of the input object
    console.log(event.target.value);
})